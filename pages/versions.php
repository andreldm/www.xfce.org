<?php
  /**
   * Always check the version number in the download page! It should open
   * a working link on archive.xfce.org!
   **/

  /* latest stable release version */
  $stable_version = '4.14';
  $stable_date = '2019-08-12';

  /* latest development version */
  $preview_version = '4.13pre3';
  $preview_date = '2019-07-28';
  $preview_visible = false /* set to true if preview is *newer* then stable */
?>
